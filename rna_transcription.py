from Bio.Seq import Seq # type: ignore

def transcribe(sequence: Seq) -> Seq:
    return sequence.transcribe()

if __name__ == '__main__':

    test_sequence = Seq('GATGGAACTTGACTACGTAAATT')
    assert transcribe(test_sequence) == 'GAUGGAACUUGACUACGUAAAUU'

    with open('input/rosalind_rna.txt') as f:
        sequence = Seq(f.read().strip())

    print(transcribe(sequence))
