import re
import typing

SEQUENCE = re.compile(r'Rosalind_\d{1,4}([ACGT]+)')

def parse_sequences(fasta_str: str) -> typing.List[str]:
    return [match.group(1) for seq in fasta_str.replace(' ', '').replace('\n', '').split('>')
        if (match := SEQUENCE.match(seq))
    ]


class BasePair:

    def __init__(self):
        self.A, self.C, self.G, self.T = 0, 0, 0, 0

    def __repr__(self) -> str:
        return f'{self.A} {self.C} {self.G} {self.T}'

    def count(self, nucleotide: str) -> None:
        setattr(self, nucleotide, getattr(self, nucleotide) + 1)

    def consensus(self) -> str:
        return max('ACGT', key=lambda n: getattr(self, n))


def consensus_and_count(sequences: typing.List[str]) -> None:
    counts = [BasePair() for _ in sequences[0]]

    for sequence in sequences:
        for i, bp in enumerate(sequence):
            counts[i].count(bp)

    print(''.join(i.consensus() for i in counts))
    for nucleotide in 'ACGT':
        print(nucleotide + ': ', end=' ')
        print(' '.join(str(getattr(count, nucleotide)) for count in counts))



if __name__ == '__main__':

    with open('input/rosalind_cons.txt') as f:
        fasta_str = f.read().strip()
        sequences = parse_sequences(fasta_str)
        consensus_and_count(sequences)
